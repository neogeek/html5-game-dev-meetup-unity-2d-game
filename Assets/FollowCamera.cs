﻿using UnityEngine;

public class FollowCamera : MonoBehaviour {

	public Transform player;

	void Update () {

		gameObject.transform.position = new Vector3(
			player.transform.position.x,
			0,
			-10
		);

	}

}
