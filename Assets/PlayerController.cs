﻿using UnityEngine;

public class PlayerController : MonoBehaviour {

	private float horizontalSpeed = 10.0f;
	private float jumpForce = 700.0f;

	private Rigidbody2D rb;

	void Start () {

		rb = gameObject.GetComponent<Rigidbody2D>();

	}

	void Update () {

		bool jumpPressed = Input.GetKeyDown("space");

		float moveHorizontal = Input.GetAxis("Horizontal");

		rb.velocity = new Vector2(moveHorizontal * horizontalSpeed, rb.velocity.y);

		if (jumpPressed) {

			rb.AddForce(new Vector2(0, jumpForce));

		}

	}

	void OnTriggerEnter2D (Collider2D other) {

		if (other.gameObject.name.Contains("coin")) {

			Destroy(other.gameObject);

		}

	}

}
